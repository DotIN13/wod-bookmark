//-----------------------------------------------------------------------------
// [WoD] Bookmarks
// Copyright (c) Jim Zhai
//-----------------------------------------------------------------------------
// ==UserScript==
// @name          Wod bookmarks
// @author        Jim Zhai
// @namespace     org.toj
// @version       0.3
// @description   Provide shortcuts to frequently used pages
// @updateURL		  https://github.com/jimraynor0/wod/raw/master/bookmarks.user.js
// @downloadURL		https://github.com/jimraynor0/wod/raw/master/bookmarks.user.js
// @include       http*://*.world-of-dungeons.*/*
// ==/UserScript==

var links = {
    // 在这里列出你喜欢的链接，格式是：
    // 名字: "链接",
    // 注意这里的逗号、引号和冒号必须是半角(英文输入法下打出来的)，最后一行不需要逗号，其他行必须以逗号结尾
    Strategy: "/wod/spiel/forum/viewforum.php?id=14975&cur_cat_id=7191&board=gruppe",
    Academy: "/wod/spiel/forum/viewforum.php?id=15020&cur_cat_id=7191&board=gruppe",
    Inn: "/wod/spiel/forum/viewforum.php?id=14976&cur_cat_id=7191&board=gruppe",
    Rep82: "/wod/spiel/forum/viewtopic.php?pid=15943946&board=kein",
    StatsDef: "/wod/spiel/forum/viewtopic.php?pid=15725876&board=kein",
    Sets: "/wod/spiel/forum/viewtopic.php?id=1431872&board=kein",
    Enchant: "wod/spiel/forum/viewtopic.php?id=31880&board=gruppe",
    Runes: "/wod/spiel/forum/viewtopic.php?pid=15931483&board=kein",
    Golden: "/wod/spiel/forum/viewtopic.php?pid=15868997&board=kein",
    Dungeons: "/wod/spiel/forum/viewtopic.php?pid=15875563&board=kein",
    Quests: "/wod/spiel/forum/viewtopic.php?pid=15874911&board=kein",
    Hidden: "/wod/spiel/forum/viewtopic.php?pid=15725897&board=kein",
    Challenge: "/wod/spiel/forum/viewtopic.php?pid=15883556&board=kein",
    BBcode: "/wod/spiel/forum/viewtopic.php?pid=15830676&board=kein",
    Index: "/wod/spiel/forum/viewtopic.php?pid=15934557&board=kein",
    Handbook: "/wod/spiel/forum/viewtopic.php?id=30602&board=gruppe"
};

var menuItems = $('<div class="menu-1-body"></div>');
$.each(links, function(label, link) {
    menuItems.append('<a href="' + link + '" class="menu-2-caption" style="user-select: auto;">' + label + '</a>');
});

$('.menu-vertical').append('<div class="menu-between"></div>');
$('.menu-vertical').append($('<div class="menu-1" id="bookmarks"/>')
                           .append('<a href="/wod/spiel/dungeon/dungeon.php?path=.8" class="menu-1-caption" onclick="return menuOnClick(this,\'\',\'library\',\'\');">快速链接<span class="menu-1-arrow closed"></span></a>')
                           .append(menuItems));
